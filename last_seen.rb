require "time"
require 'pstore' 


require 'action_view'
require 'active_support/core_ext/numeric'
include ActionView::Helpers::DateHelper


module Cinch
  module Plugins
    class LastSeen
      include Cinch::Plugin

      set :prefix, /^%/

      def initialize(*args)
        super
	@database = PStore.new('seen.db', true)
      end

      listen_to :channel, method: :log_message
      def log_message(m)
        return unless log_channel?(m.channel)
	return if m.message =~ /^%seen\s+#{m.user.nick}$/

	@database.transaction do |users|
	    users[m.user.nick.downcase] = { :nick => m.user.nick, :message => m.message, :time => Time.now }
	end
      end

      match(/seen (.+)/, method: :check_nick)
      def check_nick(m, nick)

	message = nil

	@database.transaction(true) do |users|
          message = users[nick.downcase]
	end

        if message
          time = distance_of_time_in_words(message[:time], Time.now, false)
          m.reply "#{nick} was last seen #{time} ago: #{message[:message]}", true
        else
          m.reply "I haven't seen #{nick}, sorry.", true
        end
      end

      private
      def log_channel?(channel)
        # we log a channel if:
        # - we have an explicit list of allowed channels and the
        #   channel is included
        # - we have an explicit list of disallowed channels and the
        #   channel is not included
        # - we don't have either list
        channel = channel.to_s
        our_config = config[:channels] || {}

        if our_config[:include]
          return our_config[:include].include?(channel)
        elsif our_config[:exclude]
          return !our_config[:exclude].include?(channel)
        else
          return true
        end
      end
    end
  end
end
