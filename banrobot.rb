#!//home/ktemkin/.rvm/rubies/ruby-2.0.0-p195/bin/ruby

require 'cinch'
require 'cinch/plugins/identify'
require 'pstore'
require 'sinatra'
require 'chronic_duration'
require_relative 'logging.rb'
require_relative 'history.rb'
require_relative 'last_seen.rb'
require_relative 'auth.rb'

MAX_ALLOWED_BANS = 115 
#SERVER = 'irc.nightstar.net'
SERVER = 'irc.ircrelay.com'
SERVER_PORT = 6697
CHANNELS = ['#twokinds']
NICK = 'BanRobot'
REBAN_MESSAGE = 'Sorry, you\'re still banned.'

#
# Create or load the ban database.
#
$database = PStore.new('bans.db', true)

#
# Create or load the unban database.
#
$unbans = PStore.new('unbans.db', true)

#
# Create or load a database of all hostmasks.
#
$hostmasks = PStore.new('hostmasks.db', true)


#Configure the IRC bot itself.
bot = Cinch::Bot.new do 

  #Set up the robot to join the appropriate channels.
  configure do |c|
    c.server          = SERVER
    c.channels        = CHANNELS
    c.nick            = NICK
    c.user            = BOT_USERNAME 
    c.ssl.use         = true
    c.password        = BOT_NETWORK_PASSWORD
    c.port            = SERVER_PORT
    c.plugins.plugins = [Cinch::Plugins::Identify, Cinch::Logging, Cinch::History]

    c.plugins.options[Cinch::Plugins::Identify] = {
      :username => 'BanRobot',
      :password => BOT_PASSWORD,
      :type => :nickserv
    }
    c.plugins.options[Cinch::Logging] = {
      :logfile => '/home/ktemkin/public.log'
    }
    c.plugins.options[Cinch::History] = {
      :mode => :max_messages,
      :max_messages => 10,
      :max_age => 5 * 60
    }

  end

  #
  # Create the helper functions.
  #
  helpers do

    #
    # True iff the user has permission to ban people.
    #
    def has_permission?(message, explain=true) 
      if message.channel.opped?(message.user) || message.channel.half_opped?(message.user)
        return true
      else
        message.user.msg "You must have at least half-ops to do that." if explain
        return false
      end
    end

    #
    # Determine the mask for the given nickname.
    #
    def nick_to_mask(channel, nick)
      return nil unless channel.has_user?(nick)

      #Get the user's mask, in bannable format.
      to_ban = User(nick)
      return to_ban.mask('*!*@%h')

    end

    #
    # Returns true iff the mask is in the bot's banlist.
    #
    def mask_in_banlist?(channel, mask) 
      $database.transaction(true) do |channels|
        return false unless channels.root?(channel.name)
        return channels[channel.name].has_key?(mask.to_s) 
      end
    end


    #
    # Returns information about the user's ban, or false if the user isn't banned.
    #
    def ban_information(channel, user)
      $database.transaction(true) do |channels|
        return false unless channels.root?(channel.name)

	#Search the list of all banned users, and see if the user matches any of
        #the stored masks.
        return channels[channel.name].find do |mask, value| 
            user.match(mask) rescue next
        end

      end
    end


    #
    # Adds a ban to the robot's memory.
    #
    def add_ban_to_memory(channel, mask, reason=nil)
      $database.transaction do |channels|
        channels[channel.name] ||= {}
        channels[channel.name][mask] = "#{Time.now.to_s}: " + (reason || "No reason specified.")
      end
    end

    #
    # Convenience function which updates the user's ban reason.
    #
    def update_ban_reason(channel, mask, reason)
      $database.transaction do |channels|
        channels[channel.name] ||= {}
        channels[channel.name][mask] = reason
      end
    end

    #
    # Removes a ban from the robot's memory.
    # 
    def remove_ban_from_memory(channel, to_unban)
      $database.transaction do |channels|
        channels[channel.name] ||= {}
        channels[channel.name].each {|key, value| channels[channel.name].delete(key) if Cinch::Mask.from(key).match(to_unban) }
      end
    end

    #
    # Return the number of bans in memory.
    #
    def ban_count(channel)
      $database.transaction(true) do |channels|
        return 0 unless channels.root?(channel.name)
        return channels[channel.name].count
      end
    end

    #
    # Determines if the banlist is more than the specified max quantity.
    #
    def banlist_is_full?(channel)
      channel.sync_modes
      channel.bans.count >= MAX_ALLOWED_BANS
    end

    #
    # Adds the first ban in the channel to the bot's banlist, and out of the channel's banlist.
    #
    def move_first_ban_to_bot(channel)
      ban_to_move = channel.bans.last
      add_ban_to_memory(channel, ban_to_move.mask, "Pushed in from full channel banlist.") if not mask_in_banlist?(channel, ban_to_move.mask)
      channel.unban(ban_to_move.mask)
    end

    #
    # Ban the given user. If a nickname is provided, the user is also kicked.
    #
    def ban_user_on_channel(channel, mask, reason = nil, nick = nil)

      add_ban_to_memory(channel, mask, reason) unless mask_in_banlist?(channel, mask)
      move_first_ban_to_bot(channel) while banlist_is_full?(channel)

      #Kickban the user.
      channel.ban(mask)
      channel.kick(nick, reason) if nick

    end

    #
    # Unban the given user.
    #
    def unban_user_on_channel(channel, mask)
      remove_ban_from_memory(channel, mask)
      channel.unban(mask)
    end

    #
    # Schedules a user for an automatic unban at the scheduled time.
    #
    def schedule_unban_on_channel(channel, mask, time) 
      #Add the user to the unbane database.
      $unbans.transaction do |channels|
        channels[channel.name] ||= {}
        channels[channel.name][mask] = time
      end
    end

    #
    # Enforces all scheduled unbans.
    #
    def handle_time_ban_expiration(channel)

      #Get the current unix time.
      time = Time.now.to_i

      #Access the unban database...
      $unbans.transaction do |channels|

        #Store a list of masks which have been unbanned during this transaction.
        to_unban = []

        #Check each of the bans to see if it has expired.
        channels[channel.name].each do |mask, unban_time| 
          to_unban << mask if time > unban_time          
        end

        #For each of the masks to unban...
        to_unban.each do |mask| 

          #... remove it from the unban database...
          channels[channel.name].delete(mask)

          #... and remove the ban from the channel.
          unban_user_on_channel(channel, mask) 

        end
      end

    end

    #
    # Handles the raw process of banning a given user.
    #
    def handle_user_ban(m, to_ban, reason, channel=m.channel, nick=nil, unban_at=nil)
      return if to_ban == bot.nick 

      #Figure out who to ban. If we have a nickname, convert it to a mask.
      unless to_ban.include?('@')
        nick = to_ban
        to_ban = nick_to_mask(m.channel, to_ban) 
      end

      #If we couldn't figure out which user to ban, abort.
      unless to_ban
        m.user.msg "I couldn't figure out what user to ban. Are you sure they're in the channel?"
        m.user.msg "If they're not in the channel, you must ban them by hostmask."
        return
      end

      #Ban the user.
      ban_user_on_channel(m.channel, to_ban, reason, nick)

      #And schedule their unbanning.
      schedule_unban_on_channel(m.channel, to_ban, unban_at) if unban_at

    end


    #
    # Handle a ban-request message.
    # 
    def handle_ban_request(m, to_ban, reason, channel = m.channel)
      return unless has_permission?(m) 
      handle_user_ban(m, to_ban, reason, channel)
    end

    #
    # Handles a time-ban request message.
    #
    def handle_time_ban_request(m, to_ban, timespan, reason, channel = m.channel)
      return unless has_permission?(m) 

      # If we couldn't figure out a ban time, alert the user.
      unless timespan
        m.user.msg "I'm usually pretty good about these, but that time just doesn't make sense."  
        return
      end

      #Determine the unix timestamp when the ban will be over.
      end_time = Time.now.to_i + timespan

      #Ban the user...       
      handle_user_ban(m, to_ban, reason, channel, nil, end_time)

    end


    #
    # Handle unban requests.
    #
    def handle_unban_request(m, to_unban)
      return unless has_permission?(m)
      unban_user_on_channel(m.channel, to_unban)
    end

    #
    # Logs the given user's hostmask, for later retreival.
    # 
    def log_user_hostmask(user)
      #Add the user to the host/nick database.
      $hostmasks.transaction do |info|
        info[:mask] ||= {}
        info[:mask][user.mask.to_s] = user.nick
        info[user.nick]             = user.mask.to_s
      end
    end

    #
    # Reads the user's nickname or hostmask from the opposing
    # piece of information.
    #
    def get_user_info(mask_or_nick)
      if mask_or_nick.include?('.')
        get_user_info_from_mask(mask_or_nick) 
      else 
        get_user_info_from_nick(mask_or_nick)
      end
    end

    #
    # Retreives a user's nickname and mask from the provided nickname. 
    #
    def get_user_info_from_mask(mask) 

      #Convert the mask string into a Cinch::Mask object.
      mask = build_mask(mask)

      #And search the database for something that matches the mask.
      $hostmasks.transaction(true) do |info|
        return nil unless info[:mask]

        #Try to find a piece of information that matches the user mask.
        return info[:mask].find do |db_mask, value| 
            mask.match(db_mask) rescue next
        end

      end

    rescue
      return nil
    end 


    #
    # Retreives a user's nickname and mask 
    #
    def get_user_info_from_nick(nick) 
      $hostmasks.transaction(true) { |info| return info[nick], nick }
    end 

  end

  #
  # Initialization; performed when the bot finishes connecting.
  #
  on :connect do 

    #Every 60 seconds, enforce all timed unbans.
    Timer(60) do 
      CHANNELS.each { |c| handle_time_ban_expiration(Channel(c)) }
    end
  end

  #
  # Time-ban users when specified.
  #
  on :message, /^%ban (.+?) for (.+)/ do |m, to_ban, timespan|
    seconds = ChronicDuration.parse(timespan)
    handle_time_ban_request(m, to_ban, seconds, "Banned for about #{ChronicDuration.output(seconds)} by #{m.user.nick}.")
  end

  #
  # Ban users when specified.
  #
  on :message, /^%ban (.+?) (.+)/ do |m, to_ban, reason|
    next if reason.start_with?('for ')
    handle_ban_request(m, to_ban, "#{reason} (#{m.user.nick})")
  end

  #
  # Ban users without a provided message.
  #
  on :message, /^%ban (.+?)$/ do |m, to_ban|
    next if to_ban.include?(' ')
    handle_ban_request(m, to_ban, "No reason given. (#{m.user.nick})")
  end

  #
  # Unban users, when requested.
  # 
  on :message, /^%unban (.+)$/ do |m, mask|
    handle_unban_request(m, mask)
  end

  #
  # Responds with the total amount of bans.
  #
  on :message, /^%bancount$/ do |m|
    m.reply "There are #{m.channel.bans.count} bans in the channel's memory, and #{ban_count(m.channel)} in mine."
  end

  #
  # Eater egg, for Kit.
  #
  on :message, /^%loveme$/ do |m|
    if m.user.nick.downcase == "kit"
      m.reply "Forever!"
    else
      m.reply "Get a job!"
    end
  end

  #
  # Provide whowas functionality.
  #
  on :message, /^%whowas (.*)$/i do |m, query| 
    mask, nick = get_user_info(query)
   
    if mask
      m.safe_reply("#The first matching user for #{query} was #{nick} (#{mask}).")
      m.safe_reply("For more detailed information, see http://bot.ktemkin.com/hostmasks/#{CGI.escape(query)}")
    else 
      m.safe_reply("I can't figure out who #{query} was.")
    end

  end

  #
  # Provide banreason functionality.
  #
  on :message, /^%banreason (.*)$/i do |m, query| 
    mask, reason = ban_information(m.channel, build_mask(query.strip, true))
    m.safe_reply(mask ? "#{query} was banned on #{reason}" : "#{query} doesn't appear to be banned.")
  end

  #
  # If a banned user rejoins a channel, ban them again.
  #
  on :join do |join|
    next if join.user.nick == bot.nick
    banned_mask, reason = ban_information(join.channel, join.user)
    handle_user_ban(join, banned_mask.to_s, REBAN_MESSAGE, join.channel, join.user.nick) if banned_mask
    log_user_hostmask(join.user)
  end

  #
  # If a user is banned, add then to the bot's banlist.
  #
  on :ban do |m, ban|
    next if ban.by == bot.nick
    ban_user_on_channel(m.channel, ban.mask, "Manual ban by #{ban.by}.")
  end

  #
  # If a user is unbanned, remove them from the bot's banlist.
  #
  on :unban do |m, ban|
    next if ban.by == bot.nick
    unban_user_on_channel(m.channel, ban.mask)
  end

  #
  # Keep track of the last kick message for each user.
  #
  on :kick do |m|
    next if m.user.nick == bot.nick

    # Get the user information for the kicked user.
    target = User(m.params[1])
    
    # Get the ban information for the current user.
    banned_mask, reason = ban_information(m.channel, target)

    # If this user wasn't banned, don't log the kick reason.
    next unless banned_mask

    # Update the internal logs, indicating the kick reason
    update_ban_reason(m.channel, banned_mask, "#{reason} [Kicked afterwards by #{m.user.nick}: #{m.message}]")

  end

end

#
# Iterates over each ban for the given channel.
#
def each_channel_in_database(database, channel)
  database.transaction(true) do |channels|

    #If we have bans for the channel...
    if channels.root?(channel)
      channels[channel].each { |pair| yield pair }
    end

  end
end

#
# Iterates over each of the bans for a given channel.
#
def each_ban_for_channel(channel, &block)
  each_channel_in_database($database, channel, &block)
end

#
# Iterates over each of the pending unbans for a given channel.
#
def each_unban_for_channel(channel, &block)
  each_channel_in_database($unbans, channel, &block)
end


#
# Display a list of nick-mask mappings.
#
get '/hostmasks' do 
  message = '<table style="font-family: monospace;">'
  $hostmasks.transaction(true) do |info| 
    next unless info[:mask]
    info[:mask].each { |mask, nick| message << "<tr><td>#{mask}&nbsp;&nbsp;</td><td>#{nick}</td></tr>" }
  end
  message + '</table>'
end


#
# Display a list of nick-mask mappings.
#
get '/hostmasks/:filter' do 
  message = '<table style="font-family: monospace;">'
  $hostmasks.transaction(true) do |info| 

    #If the robot has not stored any hostmasks, create an empty table.
    next unless info[:mask]

    #Create a mask object from the given filter.
    query_mask = build_mask(params[:filter])

    #Otherwise, add each of the relevant hostmasks.
    info[:mask].each do |mask, nick| 
     
      #If neither the mask nor the nick match the filter, skip incliding the given mapping.
      next unless query_mask.match(mask) or mask.to_s.include?(params[:filter]) or nick.include?(params[:filter]) 
      
      #Otherwise, add it to the list. 
      message << "<tr><td>#{mask}&nbsp;&nbsp;</td><td>#{nick}</td></tr>" 
    end
  end
  message + '</table>'
end

#
# Display a list of banned user when queried via HTTP.
#
get '/:name' do 
  message = '<table style="font-family: monospace;">'
  each_ban_for_channel("\##{params[:name]}") { |mask, reason| message << "<tr><td>#{mask}&nbsp;&nbsp;</td><td>#{reason}</td></tr>" }
  message + '</table>'
end

#
# Display a list of pending unbans.
#
get '/:name/unbans' do 

  #Get the current unix time.
  timestamp = Time.now.to_i

  message = '<table style="font-family: monospace;">'

  #Iterate over each of the pending unbans.
  each_unban_for_channel("\##{params[:name]}") do |mask, unban_time| 

    time_left = if timestamp >= unban_time 
      "About to unban..."
    else
      ChronicDuration.output(unban_time - timestamp)
    end
  
    message << "<tr><td>#{mask}&nbsp;&nbsp;</td><td>#{time_left}</td></tr>" 
  end

  message + '</table>'
end


#
# Display a filtered list of bans.
#
get '/:name/:filter' do 
  message = '<table style="font-family: monospace;">'
 
  #Iterate over each of the channels' bans...
  each_ban_for_channel("\##{params[:name]}") do |mask, reason|

    # If the reason doesn't include the filter word, skip it.
    next unless reason.downcase.include?(params[:filter].downcase)
   
    #And print the given message.
    message << "<tr><td>#{mask}&nbsp;&nbsp;</td><td>#{reason}</td></tr>" 

  end
  message + '</table>'
end


get '/' do
  'Suffix a channel name to the URL, without the #, to see its banlist.'
end

#
# Attempts to create a Cinch mask object from the (potentially partial) hostmask string.
#
def build_mask(mask, allow_nick = false) 

  #If the "allow nickname" option is set, and this doesn't look like a hostmask,
  #convert it to a nickname-based mask.
  mask = "#{mask}!*@*" if allow_nick and not mask.include?('.')

  #If this is a partial mask, try our best to convert it to a mask
  #mld.
  mask = "*!*@#{mask}" unless mask.include?('@')
  mask = "*!#{mask}" unless mask.include?('!')

  #Finally, Convert the user's string into a Mask object.
  return Cinch::Mask.from(mask)

end

set :environment, :production
set :bind, '0.0.0.0'

Thread.new { Sinatra::Application.run! }
bot.start

